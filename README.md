# TranscribeSharp-Preview
Transcribe# is a program designed to aid with the transcription of audio files. While you type using the program of your choice, Transcribe# allows you to control the audio playback without switching windows. This program is based in large part on the open source project PracticeSharp by Yuval Naveh.

This repository is intended to be temporary (until I set up a different repository with a better source control workflow).

TranscribeSharp is built in large part on PracticeSharp by Yuval Naveh (https://code.google.com/p/practicesharp/), with all of its associated dependencies.
The LowLevelHooks code by Curtis Rutland (https://github.com/curtisrutland/LowLevelHooks) is also incorporated.

For now, I intend for all of my contributions to be licensed under the LGPL (the same license as PracticeSharp). 

Please pardon the mess as I learn git and the community standards for github and BitBucket.