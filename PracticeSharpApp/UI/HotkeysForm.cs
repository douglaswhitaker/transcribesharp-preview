﻿// When using try/catch when the program opens, it fails to register a hotkey
// To ameliorate this, I'm trying to create a new form that will eventually handle all of the hotkeys
// (including setting/changing them for each use... later)

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GlobalHotkeys;

namespace BigMansStuff.PracticeSharp.UI
{
    public partial class HotkeysForm : Form
    {
        # region Global Hotkey Code

        //MainForm mf = new MainForm();

        //GlobalHotkey myghk = null;

        public MainForm MF { get; set; }

        /* try
            {

                ghk = new GlobalHotkey((Modifiers)System.Windows.Forms.Keys.Alt,
                                       (Keys)System.Windows.Forms.Keys.Oemcomma, this, true);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show(exc.Message);
            } */

        /*protected override void WndProc(ref Message m)
        {
            var hotkeyInfo = HotkeyInfo.GetFromMessage(m);
            if (hotkeyInfo != null) HotkeyProc(hotkeyInfo);
            base.WndProc(ref m);
        }*/

        /*private void HotkeyProc(HotkeyInfo hotkeyInfo)
        {
            // GHK events go here

            //if (hotkeyInfo.Key == (Keys)'A')
            //{
            //    logTextBox.Text += string.Format("Frank Underwood", Environment.NewLine);
            //}
            //else if (hotkeyInfo.Key.Equals("B"))
            //{
            //    logTextBox.Text += string.Format("Claire Underwood", Environment.NewLine);
            //}
            //else
            //{
            //    logTextBox.Text += string.Format("Heather Dunbar", Environment.NewLine);
            //}

            logTextBox.Text += string.Format("{0} : Hotkey Proc! {1}, {2}{3}",
                                    DateTime.Now.ToString("hh:MM:ss.fff"),
                                    hotkeyInfo.Key, hotkeyInfo.Modifiers,
                                    Environment.NewLine);
            logTextBox.Select(logTextBox.Text.Length, 0);

            /*if (hotkeyInfo.Key == (Keys)System.Windows.Forms.Keys.Oemcomma)
            {
                if (!m_jumpMode)
                {
                    m_jumpMode = true;
                    m_preJumpStatus = m_practiceSharpLogic.Status;

                    if (m_preJumpStatus == PracticeSharpLogic.Statuses.Playing)
                    {
                        m_practiceSharpLogic.Pause();
                    }
                }

                JumpBackward();
                //e.Handled = true;
            }*/

        //}*/

        public HotkeysForm(MainForm mf)
        {
            InitializeComponent();
            //MainForm mf = new MainForm();
            MF = mf;
            //myghk = MF.ghk;
        }

        /*private void button1_Click(object sender, EventArgs e)
        {
            //if (ghk != null) ghk.Unregister();
            if (string.IsNullOrWhiteSpace(hotkeyTextBox.Text))
            {
                MessageBox.Show(Resources.NoHotkeyErrorMessage);
                return;
            }
            var key = (Keys)Enum.Parse(typeof(Keys), hotkeyTextBox.Text.ToUpper());
            var mod = Modifiers.NoMod;
            if (altCheckBox.Checked) mod |= Modifiers.Alt;
            if (ctrlCheckBox.Checked) mod |= Modifiers.Ctrl;
            if (shiftCheckBox.Checked) mod |= Modifiers.Shift;
            if (winCheckBox.Checked) mod |= Modifiers.Win;

            try
            {
                this.MF.setGHK(new GlobalHotkey(mod, key, 0, MF, true));
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show(exc.Message);
            }*/

            /*try
            {
                mf.setGHK(new GlobalHotkey((Modifiers)System.Windows.Forms.Keys.Alt,
                    (Keys)System.Windows.Forms.Keys.Oemcomma, this, true));
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show(exc.Message);
            }*/
        /*}*/

        #endregion
        
        private void button2_Click(object sender, EventArgs e)
        {
            /* Default ExpressScribe Hotkeys
             *
             * We'll also use these values as the IDs (hkID) for the hotkeys
             * 
             * F2 Play Slow Speed
             *      Alt + Shift + S
             * F3 Play Fast Speed
             *      Alt + Shift + F
             * F4 Stop
             * F5 Open Express Scribe
             * F6 Minimize Express Scribe
             * F7 Rewind
             *      Alt + Shift + ,
             * F8 Fast Forward
             *      Alt + Shift + .
             * F9 Play
             *      Alt + Shfit + Space
             * F10 Play Real Speed 
             */

            setDefaultHotkeys();
            
        }

        public void setESHotkeys()
        {
            // Slow -- hkID: 2
            // Alt + Shift + S
            try
            {
                this.MF.setGHK(Modifiers.NoMod, System.Windows.Forms.Keys.F2, 2, MF, true);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show("Error F2");
            }

            // Fast -- hkID: 3
            // Alt + Shift + F
            try
            {
                this.MF.setGHK(Modifiers.NoMod, System.Windows.Forms.Keys.F3, 3, MF, true);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show("Error F3");
            }
            // Stop -- hkID: 4
            // F4
            try
            {
                this.MF.setGHK(Modifiers.NoMod, System.Windows.Forms.Keys.F4, 4, MF, true);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show("Error F4");
            }

            // Open Program -- hkID: 5
            // F5
            /*try
            {
                this.MF.setGHK(new GlobalHotkey(Modifiers.NoMod, System.Windows.Forms.Keys.F5, 5, MF, true), 5);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show(exc.Message);
            }*/

            // Minimize Program -- hkID: 6
            // F6
            /*try
            {
                this.MF.setGHK(new GlobalHotkey(Modifiers.NoMod, System.Windows.Forms.Keys.F6, 6, MF, true), 6);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show(exc.Message);
            }*/


            // Rewind -- hkID: 7
            // Alt + Shift + ,
            try
            {
                this.MF.setGHK(Modifiers.NoMod, System.Windows.Forms.Keys.F7, 7, MF, true);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show("Error F7");
            }

            // Fast Forward -- hkID: 8
            // Alt + Shift + .
            try
            {
                this.MF.setGHK(Modifiers.NoMod, System.Windows.Forms.Keys.F8, 8, MF, true);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show("Error F8");
            }

            // Play -- hkID: 9
            // Alt + Shift + Space
            try
            {
                this.MF.setGHK(Modifiers.NoMod, System.Windows.Forms.Keys.F9, 9, MF, true);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show("Error F9");
            }

            // Play 1.0 speed -- hkID: 10
            // F10
            /*try
            {
                this.MF.setGHK(new GlobalHotkey(Modifiers.NoMod, System.Windows.Forms.Keys.F10, 10, MF, true), 10);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show("Error F10");
            }*/
        }

        public void setDefaultHotkeys()
        {
            // Slow -- hkID: 2
            // Alt + Shift + S
            try
            {
                this.MF.setGHK(Modifiers.Alt | Modifiers.Shift, System.Windows.Forms.Keys.S, 2, MF, true);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show("Error Slow");
            }

            // Fast -- hkID: 3
            // Alt + Shift + F
            try
            {
                this.MF.setGHK(Modifiers.Alt | Modifiers.Shift, System.Windows.Forms.Keys.F, 3, MF, true);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show("Error Fast");
            }
            // Stop -- hkID: 4
            // F4
            try
            {
                this.MF.setGHK(Modifiers.NoMod, System.Windows.Forms.Keys.F4, 4, MF, true);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show("Error Stop");
            }

            // Open Program -- hkID: 5
            // F5
            /*try
            {
                this.MF.setGHK(new GlobalHotkey(Modifiers.NoMod, System.Windows.Forms.Keys.F5, 5, MF, true), 5);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show(exc.Message);
            }*/

            // Minimize Program -- hkID: 6
            // F6
            /*try
            {
                this.MF.setGHK(new GlobalHotkey(Modifiers.NoMod, System.Windows.Forms.Keys.F6, 6, MF, true), 6);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show(exc.Message);
            }*/


            // Rewind -- hkID: 7
            // Alt + Shift + ,
            try
            {
                this.MF.setGHK(Modifiers.Alt | Modifiers.Shift, System.Windows.Forms.Keys.Oemcomma, 7, MF, true);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show("Error Rewind");
            }

            // Fast Forward -- hkID: 8
            // Alt + Shift + .
            try
            {
                this.MF.setGHK(Modifiers.Alt | Modifiers.Shift, System.Windows.Forms.Keys.OemPeriod, 8, MF, true);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show("Error Fast Forward");
            }

            // Play -- hkID: 9
            // Alt + Shift + Space
            try
            {
                this.MF.setGHK(Modifiers.Alt | Modifiers.Shift, System.Windows.Forms.Keys.Space, 9, MF, true);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show("Error Play");
            }

            // Play 1.0 speed -- hkID: 10
            // F10
            try
            {
                this.MF.setGHK(Modifiers.NoMod, System.Windows.Forms.Keys.F10, 10, MF, true);
            }
            catch (GlobalHotkeyException exc)
            {
                MessageBox.Show("Error Play 1.0");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            setESHotkeys();
        }

        private void HotkeysForm_Load(object sender, EventArgs e)
        {

        }
    }
}
