﻿#region © Copyright 2015 Douglas Whitaker, Transcribe Sharp. LGPL.
/* Transcribe Sharp
    
    This file was modified from Practice Sharp (version 1.6.4), ©
    2010 by Yuval Naveh which is licensed under the LGPL.
 
    © Copyright 2015, Douglas Whitaker.
     All rights reserved.
 
    This file is part of Transcribe Sharp.

    Transcribe Sharp is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Transcribe Sharp is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser Public License for more details.

    You should have received a copy of the GNU Lesser Public License
    along with Transcribe Sharp.  If not, see <http://www.gnu.org/licenses/>.
*/
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BigMansStuff.PracticeSharp.UI
{
    /// <summary>
    /// Practice# About Form
    /// </summary>
    public partial class AboutForm : Form
    {
        #region Construction

        /// <summary>
        /// Default Constructor
        /// </summary>
        public AboutForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Form Load Event Handler - Initialized form after it has been loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AboutForm_Load(object sender, EventArgs e)
        {
            thisVersionLabel.Text = AppVersion.ToString();
        }

        #endregion

        #region Properties

        public Version AppVersion { get; set; }

        #endregion

        #region GUI Event Handlers

        private void googleCodeProjectLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LaunchWebSite("http://code.google.com/p/practicesharp/");
        }

        private void codeProjectLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LaunchWebSite("http://www.codeproject.com/KB/audio-video/practice_sharp.aspx");
        }

        private void naudioLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LaunchWebSite("http://naudio.codeplex.com/");
        }

        private void soundTouchLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LaunchWebSite("http://www.surina.net/soundtouch/");
        }

        private void csVorbisLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LaunchWebSite("https://github.com/mono/csvorbis/");
        }

        private void libFlacLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LaunchWebSite("http://flac.sourceforge.net/");
        }

        
        #endregion

        #region Private Methods

        /// <summary>
        /// Utility function - launches the web site url in the default browser
        /// </summary>
        /// <param name="webSiteUrl"></param>
        private void LaunchWebSite(string webSiteUrl)
        {
            System.Diagnostics.Process.Start(webSiteUrl);
        }

        #endregion

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LaunchWebSite("https://github.com/curtisrutland/LowLevelHooks");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LaunchWebSite("http://douglaswhitaker.com");
        }
    }
}
