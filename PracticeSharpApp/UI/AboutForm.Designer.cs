﻿namespace BigMansStuff.PracticeSharp.UI
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutForm));
            this.closeButton = new System.Windows.Forms.Button();
            this.googleCodeProjectLinkLabel = new System.Windows.Forms.LinkLabel();
            this.aboutLabel = new System.Windows.Forms.Label();
            this.naudioLinkLabel = new System.Windows.Forms.LinkLabel();
            this.soundTouchLinkLabel = new System.Windows.Forms.LinkLabel();
            this.creditsLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.thisVersionLabel = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.codeProjectLinkLabel = new System.Windows.Forms.LinkLabel();
            this.csVorbisLinkLabel = new System.Windows.Forms.LinkLabel();
            this.libFlacLinkLabel = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.closeButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.closeButton.Location = new System.Drawing.Point(181, 251);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 0;
            this.closeButton.Text = "&Close";
            this.closeButton.UseVisualStyleBackColor = true;
            // 
            // googleCodeProjectLinkLabel
            // 
            this.googleCodeProjectLinkLabel.AutoSize = true;
            this.googleCodeProjectLinkLabel.Location = new System.Drawing.Point(12, 171);
            this.googleCodeProjectLinkLabel.Name = "googleCodeProjectLinkLabel";
            this.googleCodeProjectLinkLabel.Size = new System.Drawing.Size(154, 13);
            this.googleCodeProjectLinkLabel.TabIndex = 1;
            this.googleCodeProjectLinkLabel.TabStop = true;
            this.googleCodeProjectLinkLabel.Text = "Practice# Google Code Project";
            this.googleCodeProjectLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.googleCodeProjectLinkLabel_LinkClicked);
            // 
            // aboutLabel
            // 
            this.aboutLabel.Location = new System.Drawing.Point(12, 10);
            this.aboutLabel.Name = "aboutLabel";
            this.aboutLabel.Size = new System.Drawing.Size(200, 87);
            this.aboutLabel.TabIndex = 2;
            this.aboutLabel.Text = resources.GetString("aboutLabel.Text");
            // 
            // naudioLinkLabel
            // 
            this.naudioLinkLabel.AutoSize = true;
            this.naudioLinkLabel.Location = new System.Drawing.Point(219, 155);
            this.naudioLinkLabel.Name = "naudioLinkLabel";
            this.naudioLinkLabel.Size = new System.Drawing.Size(42, 13);
            this.naudioLinkLabel.TabIndex = 3;
            this.naudioLinkLabel.TabStop = true;
            this.naudioLinkLabel.Text = "NAudio";
            this.naudioLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.naudioLinkLabel_LinkClicked);
            // 
            // soundTouchLinkLabel
            // 
            this.soundTouchLinkLabel.AutoSize = true;
            this.soundTouchLinkLabel.Location = new System.Drawing.Point(219, 168);
            this.soundTouchLinkLabel.Name = "soundTouchLinkLabel";
            this.soundTouchLinkLabel.Size = new System.Drawing.Size(69, 13);
            this.soundTouchLinkLabel.TabIndex = 4;
            this.soundTouchLinkLabel.TabStop = true;
            this.soundTouchLinkLabel.Text = "SoundTouch";
            this.soundTouchLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.soundTouchLinkLabel_LinkClicked);
            // 
            // creditsLabel
            // 
            this.creditsLabel.AutoSize = true;
            this.creditsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creditsLabel.Location = new System.Drawing.Point(12, 125);
            this.creditsLabel.Name = "creditsLabel";
            this.creditsLabel.Size = new System.Drawing.Size(56, 17);
            this.creditsLabel.TabIndex = 5;
            this.creditsLabel.Text = "Credits:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Version:";
            // 
            // thisVersionLabel
            // 
            this.thisVersionLabel.AutoSize = true;
            this.thisVersionLabel.Location = new System.Drawing.Point(63, 96);
            this.thisVersionLabel.Name = "thisVersionLabel";
            this.thisVersionLabel.Size = new System.Drawing.Size(27, 13);
            this.thisVersionLabel.TabIndex = 7;
            this.thisVersionLabel.Text = "N/A";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::BigMansStuff.PracticeSharp.Resources.lgplv3_147x51;
            this.pictureBox2.Location = new System.Drawing.Point(280, 47);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(131, 60);
            this.pictureBox2.TabIndex = 9;
            this.pictureBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(219, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(198, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "© 2015, Douglas Whitaker";
            // 
            // codeProjectLinkLabel
            // 
            this.codeProjectLinkLabel.AutoSize = true;
            this.codeProjectLinkLabel.Location = new System.Drawing.Point(12, 184);
            this.codeProjectLinkLabel.Name = "codeProjectLinkLabel";
            this.codeProjectLinkLabel.Size = new System.Drawing.Size(146, 13);
            this.codeProjectLinkLabel.TabIndex = 11;
            this.codeProjectLinkLabel.TabStop = true;
            this.codeProjectLinkLabel.Text = "Practice# CodeProject Article";
            this.codeProjectLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.codeProjectLinkLabel_LinkClicked);
            // 
            // csVorbisLinkLabel
            // 
            this.csVorbisLinkLabel.AutoSize = true;
            this.csVorbisLinkLabel.Location = new System.Drawing.Point(219, 181);
            this.csVorbisLinkLabel.Name = "csVorbisLinkLabel";
            this.csVorbisLinkLabel.Size = new System.Drawing.Size(43, 13);
            this.csVorbisLinkLabel.TabIndex = 12;
            this.csVorbisLinkLabel.TabStop = true;
            this.csVorbisLinkLabel.Text = "Vorbis#";
            this.csVorbisLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.csVorbisLinkLabel_LinkClicked);
            // 
            // libFlacLinkLabel
            // 
            this.libFlacLinkLabel.AutoSize = true;
            this.libFlacLinkLabel.Location = new System.Drawing.Point(219, 194);
            this.libFlacLinkLabel.Name = "libFlacLinkLabel";
            this.libFlacLinkLabel.Size = new System.Drawing.Size(37, 13);
            this.libFlacLinkLabel.TabIndex = 13;
            this.libFlacLinkLabel.TabStop = true;
            this.libFlacLinkLabel.Text = "libFlac";
            this.libFlacLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.libFlacLinkLabel_LinkClicked);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(12, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 29);
            this.label3.TabIndex = 14;
            this.label3.Text = "Transcribe# is largely based on Practice#. © 2010 by Yuval Naveh.";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(13, 203);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(200, 26);
            this.label4.TabIndex = 15;
            this.label4.Text = "LowLevelHooks by Curtis Rutland is also incorporated.";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(12, 229);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(135, 13);
            this.linkLabel1.TabIndex = 16;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "LowLevelHooks on GitHub";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel2.Location = new System.Drawing.Point(277, 27);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(140, 17);
            this.linkLabel2.TabIndex = 17;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "douglaswhitaker.com";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(219, 142);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(137, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Other libraries used include:";
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(-2, 119);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(440, 2);
            this.label6.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(119, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(319, 14);
            this.label7.TabIndex = 20;
            this.label7.Text = "This is essentially a preview/alpha release. There are known bugs!";
            // 
            // AboutForm
            // 
            this.AcceptButton = this.closeButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.closeButton;
            this.ClientSize = new System.Drawing.Size(438, 286);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.linkLabel2);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.libFlacLinkLabel);
            this.Controls.Add(this.csVorbisLinkLabel);
            this.Controls.Add(this.codeProjectLinkLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.thisVersionLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.creditsLabel);
            this.Controls.Add(this.soundTouchLinkLabel);
            this.Controls.Add(this.naudioLinkLabel);
            this.Controls.Add(this.aboutLabel);
            this.Controls.Add(this.googleCodeProjectLinkLabel);
            this.Controls.Add(this.closeButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "About Transcribe#";
            this.Load += new System.EventHandler(this.AboutForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button closeButton;
        private System.Windows.Forms.LinkLabel googleCodeProjectLinkLabel;
        private System.Windows.Forms.Label aboutLabel;
        private System.Windows.Forms.LinkLabel naudioLinkLabel;
        private System.Windows.Forms.LinkLabel soundTouchLinkLabel;
        private System.Windows.Forms.Label creditsLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label thisVersionLabel;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel codeProjectLinkLabel;
        private System.Windows.Forms.LinkLabel csVorbisLinkLabel;
        private System.Windows.Forms.LinkLabel libFlacLinkLabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}